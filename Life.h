// ---------
// Life.h
// ---------

#ifndef Life_h
#define Life_h





// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector>
#include <set>

#include "LifeType.h"
#include "AbstractCell.h"
#include "ConwayCell.h"
#include "FredkinCell.h"
#include "Cell.h"
#include "Life.h"

using namespace std;


// the type of the cells.
class Life {
private:
    void initialize_cell_alive(int row, int col);
    void update_neighbors(int r, int c, bool state);
    void handle_mutation(const vector<pair<int,int>>& newly_mutated);
    Life_type _t;
    vector<vector<Cell>> _life;
    vector<vector<int>> _neighbor;
    set<pair<int,int>> _changed; //for the single pass algorithm
    int _pop;
    vector<pair<int,int>> newly_mutated = {}; //needed to handle mutations
public:
    Life():
        _t(cell),
        _life(),
        _neighbor(),
        _changed(),
        _pop()
    {}
    //Life(Life_type t,const int& r, const int& c);
    Life(Life_type t, istream& r);

    /*
    Runs a whole generation on _life.
    */
    void evolve_grid();


    /*
    Ouputs the state of the grid in the output format for the
    acceptance tests.
    */
    void print_state(ostream& w, int generation, bool last_case);
    inline void print_state(ostream& w, int generation) {
        print_state(w, generation, false);
    }



    //outputs the header for when starting a new test
    inline void print_header(ostream& w, string game_type) {
        w << "*** Life<"<< game_type<<"> "<< _life.size() - 2 << "x" << _life[0].size() - 2 << " ***" << "\n" << endl;
    }
};


extern Life life_game;

// ------------
// life_read
// ------------

/**
 */
pair<size_t, size_t> life_read (Life_type t, istream& sin, ostream& w);


// ------------
// life_eval
// ------------

/**
 */
void life_eval_print(ostream& w, int iterations, int print, bool last);


// -------------
// life_solve
// -------------

/**
 */
void life_solve (Life_type t, istream& r, ostream& w);


#endif // Life_h
