.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

FILES :=                                \
    .gitignore                          \
    life-tests                         	\
    Life.c++                           	\
    Life.h                             	\
    makefile                            \
    RunLifeCell.c++                     \
    RunLifeCell.in                      \
    RunLifeCell.out                     \
    RunLifeConway.c++                   \
    RunLifeConway.in                    \
    RunLifeConway.out                   \
    RunLifeFredkin.c++                  \
    RunLifeFredkin.in                   \
    RunLifeFredkin.out                  \
    TestLife.c++ 						\
    AbstractCell.h 						\
    ConwayCell.h 						\
    ConwayCell.c++ 						\
    FredkinCell.h 						\
    FredkinCell.c++ 					\
    Cell.h 								\
    Cell.c++ 							\



# uncomment these four lines when you've created those files
# you must replace GitLabID with your GitLabID
#    life-tests/GitLabID-RunLife.in  \
#    life-tests/GitLabID-RunLife.out \
#    Life.log                           \
#    html                                  \

life-tests:
	git clone https://gitlab.com/gpdowning/cs371p-life-tests.git life-tests

html: Doxyfile Life.h
	doxygen Doxyfile

Life.log:
	git log > Life.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	doxygen -g

RunLife: Life.h Life.c++ RunLifeCell.c++ RunLifeConway.c++ RunLifeFredkin.c++
	-cppcheck Life.c++
	-cppcheck RunLifeCell.c++
	-cppcheck RunLifeConway.c++
	-cppcheck RunLifeFredkin.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Life.c++ RunLifeCell.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ -o RunLifeCell
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Life.c++ RunLifeConway.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ -o RunLifeConway
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Life.c++ RunLifeFredkin.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ -o RunLifeFredkin


RunLife.c++x: RunLife

	./RunLifeCell < RunLifeCell.in > RunLifeCell.tmp
	-diff RunLifeCell.tmp RunLifeCell.out

	./RunLifeConway < RunLifeConway.in > RunLifeConway.tmp
	-diff RunLifeConway.tmp RunLifeConway.out

	./RunLifeFredkin < RunLifeFredkin.in > RunLifeFredkin.tmp
	-diff RunLifeFredkin.tmp RunLifeFredkin.out

TestLife: Life.h Life.c++ TestLife.c++
	-cppcheck Life.c++
	-cppcheck TestLife.c++
	g++ -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra  Life.c++ TestLife.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ -o TestLife -lgtest -lgtest_main -pthread

TestLife.c++x: TestLife
	valgrind ./TestLife
	gcov -b Life.c++ | grep -A 5 "File '.*Life.c++'"

all: RunLife TestLife

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunLifeCell
	rm -f RunLifeConway
	rm -f RunLifeFredkin
	rm -f TestLife

config:
	git config -l

ctd:
	checktestdata Test.ctd RunLifeCell.in
	checktestdata Test.ctd RunLifeConway.in
	checktestdata Test.ctd RunLifeFredkin.in

docker:
	#docker run -it -v $(PWD):/usr/life -w /usr/life gpdowning/gcc
	docker run --rm -it -v /home/beaudry/cs/classes/OOP/cs371p-life:/usr/life -w /usr/life gpdowning/gcc

format:
	astyle AbstractCell.h
	astyle Cell.h
	astyle Cell.c++
	astyle RunLifeCell.c++
	astyle ConwayCell.h
	astyle ConwayCell.c++
	astyle RunLifeConway.c++
	astyle FredkinCell.h
	astyle FredkinCell.c++
	astyle RunLifeFredkin.c++
	astyle Life.c++
	astyle Life.h
	astyle TestLife.c++

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs371p-life.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Life.c++
	git add Life.h
	-git add Life.log
	-git add html
	git add makefile
	git add RunLife.c++
	git add RunLife.in
	git add RunLife.out
	git add Test.ctd
	git add TestLife.c++
	git commit -m "another commit"
	git push
	git status

run: RunLife.c++x TestLife.c++x

scrub:
	make clean
	rm -f  Life.log
	rm -f  Doxyfile
	rm -rf life-tests
	rm -rf html
	rm -rf latex

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

versions:
	which         astyle
	astyle        --version
	@echo
	dpkg -s       libboost-dev | grep 'Version'
	@echo
	ls -al        /usr/lib/*.a
	@echo
	which         checktestdata
	checktestdata --version
	@echo
	which         cmake
	cmake         --version
	@echo
	which         cppcheck
	cppcheck      --version
	@echo
	which         doxygen
	doxygen       --version
	@echo
	which         g++
	g++           --version
	@echo
	which         gcov
	gcov          --version
	@echo
	which         git
	git           --version
	@echo
	which         make
	make          --version
	@echo
	which         valgrind
	valgrind      --version
	@echo
	which         vim
	vim           --version
