var searchData=
[
  ['cell',['Cell',['../classCell.html',1,'Cell'],['../classCell.html#a394510643e8664cf12b5efaf5cb99f71',1,'Cell::Cell()'],['../classCell.html#a8e621ee330c6ef93615bc72e85c0159a',1,'Cell::Cell(Life_type t)'],['../classCell.html#ac17cbf1dc50c7ce74edb7fdf965f0003',1,'Cell::Cell(AbstractCell *p)'],['../classCell.html#a94a54f1c668b29010d8b1662cd6fa849',1,'Cell::Cell(const Cell &amp;ref)'],['../classCell.html#a7e1f747d8e123d711ae884e316dcde82',1,'Cell::Cell(Cell &amp;&amp;)'],['../LifeType_8h.html#aa77df681edc5b042737a81e08ed36e60ace31af78b752468d203175acf1a2a4de',1,'cell():&#160;LifeType.h']]],
  ['cell_2ec_2b_2b',['Cell.c++',['../Cell_8c_09_09.html',1,'']]],
  ['cell_2eh',['Cell.h',['../Cell_8h.html',1,'']]],
  ['clone',['clone',['../classAbstractCell.html#a1a95a7ea92b3503e2f042170b6320354',1,'AbstractCell::clone()'],['../classConwayCell.html#a0fac73dc33d36053d1400430f1c980ce',1,'ConwayCell::clone()'],['../classFredkinCell.html#a05d7cd1308b23d514e207317fdf06235',1,'FredkinCell::clone()']]],
  ['conway',['conway',['../LifeType_8h.html#aa77df681edc5b042737a81e08ed36e60a650c425abe1ca7e751980b51026d0ed8',1,'LifeType.h']]],
  ['conwaycell',['ConwayCell',['../classConwayCell.html',1,'ConwayCell'],['../classConwayCell.html#aeff597ba7adcb28d4c386d075eddb196',1,'ConwayCell::ConwayCell()'],['../classConwayCell.html#a06d7b2e19da0c54576cc831aa7e8089e',1,'ConwayCell::ConwayCell(const bool &amp;state)'],['../classConwayCell.html#ab45de64d1af0778f7a672a46f34bc80b',1,'ConwayCell::ConwayCell(const AbstractCell &amp;a)']]],
  ['conwaycell_2ec_2b_2b',['ConwayCell.c++',['../ConwayCell_8c_09_09.html',1,'']]],
  ['conwaycell_2eh',['ConwayCell.h',['../ConwayCell_8h.html',1,'']]],
  ['create_5finitial',['create_initial',['../HRLife_8c_09_09.html#affb79eff77d86cec1bf6e9d00d9099c6',1,'HRLife.c++']]],
  ['current_5fgame',['current_game',['../HRLife_8c_09_09.html#a72badc4cd8fcb248678b502e01f1c03e',1,'HRLife.c++']]]
];
