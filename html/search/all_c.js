var searchData=
[
  ['population',['population',['../HRLife_8c_09_09.html#af87cd04a98544caec87030cacb90e980',1,'HRLife.c++']]],
  ['print_5fgrid',['print_grid',['../HRLife_8c_09_09.html#a3cdaee7389b02cf39c09432e2aa89f7e',1,'HRLife.c++']]],
  ['print_5fheader',['print_header',['../classLife.html#acf06e80ec3579e30ccd3541af4321319',1,'Life']]],
  ['print_5fneighbors',['print_neighbors',['../HRLife_8c_09_09.html#aded345ac826b221488df4a1ad7509c79',1,'HRLife.c++']]],
  ['print_5fstate',['print_state',['../classLife.html#a216e48f8c04a9cf7fb3518d3230b316b',1,'Life::print_state(ostream &amp;w, int generation, bool last_case)'],['../classLife.html#a4c5b623acfdb1d58db5952f70998491d',1,'Life::print_state(ostream &amp;w, int generation)']]]
];
