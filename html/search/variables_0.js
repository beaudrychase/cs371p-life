var searchData=
[
  ['_5fage',['_age',['../classFredkinCell.html#a2486375e903e4fec0a43d1b183fc1098',1,'FredkinCell']]],
  ['_5fchanged',['_changed',['../classLife.html#a28811c951bd74b9b10abeb8d184d2cff',1,'Life']]],
  ['_5fdiagonal',['_diagonal',['../classAbstractCell.html#ae70c3afc82a3dd9a4fb597347b276ef5',1,'AbstractCell']]],
  ['_5flife',['_life',['../classLife.html#a27cb04a195973a1a00b1c91f0b0ac15f',1,'Life']]],
  ['_5fneighbor',['_neighbor',['../classLife.html#a4a47157567bb8ffbd4ee573d708af865',1,'Life']]],
  ['_5fp',['_p',['../classCell.html#af8bb6306c15463d257e4775095b2f273',1,'Cell']]],
  ['_5fpop',['_pop',['../classLife.html#a6211a662babe78c543450d146575abab',1,'Life']]],
  ['_5fstate',['_state',['../classAbstractCell.html#a3bb489f0bc1aa3c925bca073b89c28be',1,'AbstractCell']]],
  ['_5ft',['_t',['../classLife.html#ad74266576f0af6f7068a87ae11d9f08f',1,'Life']]]
];
