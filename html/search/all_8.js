var searchData=
[
  ['life',['Life',['../classLife.html',1,'Life'],['../classLife.html#a6de2a371f6f778f8b4938d219390b746',1,'Life::Life()'],['../classLife.html#ad5cad3b62594330acdf6258b940eaf82',1,'Life::Life(Life_type t, istream &amp;r)']]],
  ['life_2ec_2b_2b',['Life.c++',['../Life_8c_09_09.html',1,'']]],
  ['life_2eh',['Life.h',['../Life_8h.html',1,'']]],
  ['life_5feval_5fprint',['life_eval_print',['../HRLife_8c_09_09.html#a16ca77cbd392a30b4e429668ee665cc4',1,'life_eval_print(ostream &amp;w, int iterations, int print, bool last):&#160;HRLife.c++'],['../Life_8c_09_09.html#a2c098e991815c7cce5f4e51f9bd1cc5e',1,'life_eval_print(ostream &amp;w, size_t iterations, int print, bool last):&#160;Life.c++'],['../Life_8h.html#a16ca77cbd392a30b4e429668ee665cc4',1,'life_eval_print(ostream &amp;w, int iterations, int print, bool last):&#160;HRLife.c++']]],
  ['life_5fgame',['life_game',['../Life_8c_09_09.html#ac4e7a461a4c3813664906f6efc730127',1,'life_game():&#160;Life.c++'],['../Life_8h.html#ac4e7a461a4c3813664906f6efc730127',1,'life_game():&#160;Life.c++']]],
  ['life_5fread',['life_read',['../HRLife_8c_09_09.html#ae1292e571a43f7c1bc474c6fc85df704',1,'life_read(istream &amp;sin, ostream &amp;w):&#160;HRLife.c++'],['../Life_8c_09_09.html#aa58464e04ec4b817379c9951ab4d6b42',1,'life_read(Life_type t, istream &amp;sin, ostream &amp;w):&#160;Life.c++'],['../Life_8h.html#aa58464e04ec4b817379c9951ab4d6b42',1,'life_read(Life_type t, istream &amp;sin, ostream &amp;w):&#160;Life.c++']]],
  ['life_5fsolve',['life_solve',['../HRLife_8c_09_09.html#a7fcb61de84f8242d40ab16ddeff47525',1,'life_solve(istream &amp;r, ostream &amp;w):&#160;HRLife.c++'],['../Life_8c_09_09.html#ab40f5601849862d017540da288f93aa7',1,'life_solve(Life_type t, istream &amp;r, ostream &amp;w):&#160;Life.c++'],['../Life_8h.html#ab40f5601849862d017540da288f93aa7',1,'life_solve(Life_type t, istream &amp;r, ostream &amp;w):&#160;Life.c++']]],
  ['life_5ftype',['Life_type',['../LifeType_8h.html#aa77df681edc5b042737a81e08ed36e60',1,'LifeType.h']]],
  ['lifetype_2eh',['LifeType.h',['../LifeType_8h.html',1,'']]]
];
