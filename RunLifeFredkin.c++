// --------------
// RunLife.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Life.h"

// ----
// main
// ----

int main () {
    using namespace std;
    life_solve(fredkin, cin, cout);
    return 0;
}
