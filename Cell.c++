#include "Cell.h"
#include "ConwayCell.h"
#include "FredkinCell.h"
#include "Life.h"
#include <algorithm>
using namespace std;

Cell::Cell():
    _p(new FredkinCell())
{}


Cell::Cell(Life_type t):
    _p()
{
    if(conway == t) {
        _p = new ConwayCell(false);
    } else {
        _p = new FredkinCell(false, 0);
    }
}

Cell::Cell(AbstractCell* p):
    _p(p)
{}
Cell::Cell(const Cell& ref):
    _p(ref._p->clone())
{}
Cell::Cell(Cell&& other):
    _p()
{
    swap(_p, other._p);
}

Cell::~Cell() {
    delete _p;
}

Cell& Cell::operator=(const Cell& rhs) {
    _p = rhs._p->clone();
    return *this;
}
Cell& Cell::operator=(Cell&& rhs) {
    swap(_p, rhs._p);
    return *this;

}
