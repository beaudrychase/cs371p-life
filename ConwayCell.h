#ifndef ConwayCell_h
#define ConwayCell_h


#include "AbstractCell.h"

class ConwayCell: public AbstractCell {
public:
    ConwayCell():
        AbstractCell(false, true)
    {}
    ConwayCell(const bool& state):
        AbstractCell(state, true)
    {}
    ConwayCell(const AbstractCell& a):
        AbstractCell(a, true)
    {}

    ConwayCell* clone() const;

    bool evolve(int neighbor_num);
    bool diagonal_neighbors() const;
    inline char show_state() const {
        return AbstractCell::_state? '*' : '.';
    }
};

#endif
