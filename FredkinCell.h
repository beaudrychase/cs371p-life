#ifndef FredkinCell_h
#define FredkinCell_h
#include <string>



#include "AbstractCell.h"

class FredkinCell: public AbstractCell {
private:
    int _age;
public:
    FredkinCell():
        AbstractCell(false, false),
        _age(0)
    {}
    FredkinCell(const bool& state, int age):
        AbstractCell(state, false),
        _age(age)
    {}

    FredkinCell* clone() const;
    bool evolve(int neighbor_num);
    bool diagonal_neighbors()const;
    inline char show_state() const {
        char age_char = _age < 10? std::to_string(_age)[0] : '+';
        return AbstractCell::_state? age_char : '-';
    }
};
#endif