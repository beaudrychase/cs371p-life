#include "ConwayCell.h"
using namespace std;


bool ConwayCell::evolve(int neighbor_num) {
    //a dead cell becomes a live cell, if exactly 3 neighbors are alive
    bool starting_state = AbstractCell::_state;

    if(!AbstractCell::_state && neighbor_num == 3) {
        AbstractCell::_state = true;
    }
    //a live cell becomes a dead cell, if less than 2 or more than 3 neighbors are alive
    else if(AbstractCell::_state && (neighbor_num < 2 || neighbor_num > 3)) {
        AbstractCell::_state = false;
    }
    return starting_state != AbstractCell::_state;
}

ConwayCell* ConwayCell::clone() const {
    return new ConwayCell(AbstractCell::_state);
}

bool ConwayCell::diagonal_neighbors() const {
    return AbstractCell::diagonal_neighbors();
}
