// -----------
// Life.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Life.h"

using namespace std;

Life life_game;

Life::Life(Life_type t, istream& sin):
    _t(t),
    _life(),
    _neighbor(),
    _changed(),
    _pop()
{
    size_t r;
    size_t c;
    sin >> r >> c;
    _life = vector<vector<Cell>>(r + 2, vector<Cell>(c + 2, Cell(t)));

    // current_game = vector<vector<pair<bool,int>>>(r + 2, vector<pair<bool,int>>(c + 2, pair<int,bool>{0, false}));
    _neighbor = vector<vector<int>>(r + 2, vector<int>(c + 2, 0));


    size_t on_cells;
    sin >> on_cells;



    for(size_t i = 0; i < on_cells; ++i) {
        size_t row;
        size_t col;
        sin >> row >> col;
        if(row < r && col < c && !_life[row + 1][col + 1].alive()) {
            initialize_cell_alive(row + 1, col + 1);

            _changed.insert({row + 1, col + 1});
            //if the insert added a new element then this
            //newly turned on cell hasn't been accounted for.
            ++_pop;
        }
    }

}



void Life::initialize_cell_alive(int row, int col) {
    if(_t == conway) {
        _life[row][col] = Cell(new ConwayCell(true));
    } else {
        _life[row][col] = Cell(new FredkinCell(true, 0));
    }
}


void Life::update_neighbors(int r, int c, bool state) {
    int difference = state? 1 : -1;
    //these four are all diagonal neighbors
    if(_life[r - 1][c - 1].diagonal_neighbors()) _neighbor[r - 1][c - 1] += difference;
    if(_life[r - 1][c + 1].diagonal_neighbors()) _neighbor[r - 1][c + 1] += difference;
    if(_life[r + 1][c - 1].diagonal_neighbors()) _neighbor[r + 1][c - 1] += difference;
    if(_life[r + 1][c + 1].diagonal_neighbors()) _neighbor[r + 1][c + 1] += difference;

    //we always do the orthagonal ones
    _neighbor[r -1][c] += difference;
    _neighbor[r][c -1] += difference;
    _neighbor[r][c +1] += difference;
    _neighbor[r +1][c] += difference;

}


void Life::evolve_grid() {
    for (set<pair<int,int>>::iterator it=_changed.begin(); it!=_changed.end(); ++it) {
        update_neighbors(it->first, it->second, _life[it->first][it->second].alive());
    }

    /*
    The cells from the previous round are mutated after the _changed cells
    have been accounted for so that the diagonal ones that came alive in
    the last round are not counted two times.
    */
    if(_t == cell) handle_mutation(newly_mutated);
    _changed = {}; //clear _changed
    newly_mutated = {};
    //based on the new neighbor counts, update the states of the cells.
    for(size_t r = 1; r < _life.size() - 1 ; ++r) {
        for(size_t c = 1; c < _life[0].size() - 1; ++c) {

            if(_life[r][c].evolve(_neighbor[r][c])) {
                //evolve returns whether or not the state changed
                _pop += _life[r][c].alive()? 1 : -1;
                _changed.insert({r,c});
            }
            if(_t == cell && _life[r][c].show_state() == '2') {
                //handles the strange case when cells mutate
                newly_mutated.push_back({r,c});

            }
        }
    }




}

void Life::handle_mutation(const vector<pair<int,int>>& newly_mutated) {
    for(size_t i = 0; i < newly_mutated.size(); ++i) {
        int r = newly_mutated[i].first;
        int c = newly_mutated[i].second;
        _life[r][c].mutate();

        //add the currently unaccounted for diagonal neighbors
        _neighbor[r][c] += _life[r - 1][c - 1].alive()? 1:0;
        _neighbor[r][c] += _life[r - 1][c + 1].alive()? 1:0;
        _neighbor[r][c] += _life[r + 1][c - 1].alive()? 1:0;
        _neighbor[r][c] += _life[r + 1][c + 1].alive()? 1:0;
    }
}



void Life::print_state(ostream& w, int generation, bool last_case) {
//Generation = 0, Population = 3.
    w<< "Generation = " << generation << ", Population = " << _pop << ".\n";
    for(size_t r = 1; r < _life.size() - 1 ; ++r) {
        for(size_t c = 1; c < _life[0].size() - 1; ++c) {
            char state = _life[r][c].show_state();
            if(_t == cell &&state == '2') {
                /*
                These cells haven't technically mutated yet, but
                they will mutate at the start of the next round.
                */
                state = '*';
            }
            w << state;
        }
        if(!(last_case && r == _life.size() -2)) {
            w << endl;
        }

    }

}

pair<size_t, size_t> life_read (Life_type t, istream& sin, ostream& w) {
    string game_type;
    switch(t) {
    case conway:
        game_type = "ConwayCell";
        break;
    case fredkin:
        game_type = "FredkinCell";
        break;
    default:
        game_type = "Cell";

    }

    life_game = Life(t, sin);
    life_game.print_header(w, game_type);
    size_t r;
    size_t c;

    sin >> r; //the number of turns
    sin >> c; // how many to print

    return make_pair(r, c);
}

void life_eval_print(ostream& w, size_t iterations, int print, bool last) {

    for(size_t i = 0 ; i <= iterations ; ++i) {
        if(i % print == 0) {
            life_game.print_state(w, i, last && i == iterations);
            if(!(last && i == iterations)) {
                w << endl;
            }
        }
        life_game.evolve_grid();
    }

}


void life_solve (Life_type t, istream& r, ostream& w) {

    //  read in num tests and get rid of empty line after it
    string dummy;
    getline(r, dummy);
    size_t num_tests = stoi(dummy);
    getline(r, dummy);


    for(size_t test = 0; test < num_tests; ++test) {
        pair<size_t, size_t> run_format = life_read(t, r, w);
        life_eval_print(w, run_format.first, run_format.second, test == num_tests - 1);


    }
    w << endl;
}


