// -----------
// Life.c++
// -----------


// ------------
// life_read
// ------------

/**
 * read two ints
 * @param s a string
 * @return a pair of ints, representing the beginning and end of a range, [i, j]
 */
pair<int, int> life_read (istream& sin, ostream& w);


// ------------
// life_eval
// ------------

/**
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @return the max cycle length of the range [i, j]
 */
void life_eval_print(ostream& w, int iterations, int print, bool last);

// -------------
// life_print
// -------------

/**
 * print three ints
 * @param w an ostream
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @param v the max cycle length
 */

// -------------
// life_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 */
void life_solve (istream& r, ostream& w);


void print_grid(bool last);
void print_neighbors();
int evolve();
void update_neighbors(size_t r, size_t c, bool state);
int update_whole_board();

extern vector<vector<pair<bool,int>>> current_game;
extern vector<vector<int>> neighbor_num;
extern vector<pair<size_t,size_t>> updated;

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Life.h"

using namespace std;

vector<vector<pair<bool,int>>> current_game;
vector<vector<int>> neighbor_num;
vector<pair<size_t,size_t>> updated = vector<pair<size_t,size_t>>();
int population = 0;

void create_initial() {
    for(size_t r = 1; r <current_game.size() - 1; ++r){
        for(size_t c = 1; c < current_game[0].size() - 1; ++c){
            if(current_game[r][c].first) {
                updated.push_back({r, c});
                ++population;
            }

        }
        
    }
}

pair<int, int> life_read (istream& sin, ostream& w) {
    //istringstream sin(s);
    population = 0;
    size_t r;
    size_t c;
    sin >> r >> c;
    current_game = vector<vector<pair<bool,int>>>(r + 2, vector<pair<bool,int>>(c + 2, pair<int,bool>{0, false}));
    neighbor_num = vector<vector<int>>(r + 2, vector<int>(c + 2, 0));

    w << "*** Life<Cell> "<< r << "x" << c << " ***" << "\n" << endl;

    size_t on_cells;
    sin >> on_cells;



    for(size_t i = 0; i < on_cells; ++i) {
        size_t row;
        size_t col;
        sin >> row >> col;
        bool x;
        current_game[row + 1][col + 1].first = true;
        //updated.push_back({row + 1, col + 1});
        //++population;

    }
    create_initial();

    
    sin >> r; //the number of turns
    sin >> c; // how many to print



    return make_pair(r, c);
}


void print_grid(bool last_print) {
    for(size_t r = 1; r <current_game.size() - 1; ++r){
        for(size_t c = 1; c < current_game[0].size() - 1; ++c){

            if(current_game[r][c].second >= 2){
                //print this as a conway cell
                if(current_game[r][c].first){
                    //its alive
                    cout<< "*";
                } else {
                    cout<< ".";
                }
            } else {
                if(current_game[r][c].first){
                    if(current_game[r][c].second >= 10){
                        cout << "+";
                    } else {
                        cout << current_game[r][c].second;
                    }

                } else {
                    cout << "-";
                }
            }


        }
        if(!(last_print && r == current_game.size() - 2)) {
            cout << endl;    
        }
        
    }
}

void print_neighbors() {
    for(size_t r = 1; r < neighbor_num.size() - 1; ++r){
        for(size_t c = 1; c < neighbor_num[0].size() - 1; ++c){
            cout << neighbor_num[r][c];
            
        }
        cout << endl;
    }
}

int evolve(){

    for(size_t i = 0; i < updated.size(); ++i){
        size_t r = updated[i].first;
        size_t c = updated[i].second;
        bool state = current_game[r][c].first;
        update_neighbors(r, c, state);
    }
    updated = {}; // clear updated
    return update_whole_board();
}

void add_expanded_neighbors(size_t r, size_t c) {
    if(current_game[r - 1][c - 1].first){
        neighbor_num[r][c]++;
    }
    if(current_game[r - 1][c + 1].first){
        neighbor_num[r][c]++;
    }
    if(current_game[r + 1][c - 1].first){
        neighbor_num[r][c]++;
    }
    if(current_game[r + 1][c + 1].first){
        neighbor_num[r][c]++;
    }


}

int update_whole_board() {
    int pop = 0;
    vector<pair<int,int>> new_conway = {};
// a dead cell becomes a live cell, if exactly 3 neighbors are alive
// a live cell becomes a dead cell, if less than 2 or more than 3 neighbors are alive
    for(size_t r = 1; r <current_game.size() - 1; ++r){
        for(size_t c = 1; c < current_game[0].size() - 1; ++c){
            bool current_state = current_game[r][c].first;
            bool starting_state = current_state;
            int neighbors_alive = neighbor_num[r][c];
            if(current_game[r][c].second >= 2) {
                current_game[r][c].second++;
                if(!current_state && neighbors_alive == 3){
            //if it is dead and has three alive neighbors
                    updated.push_back({r, c});
                    current_state = true;
                } else if(current_state && (neighbors_alive < 2 || neighbors_alive > 3)){
                    updated.push_back({r, c});
                    current_state = false;
                }
                current_game[r][c].first = current_state;
                if(current_state){
                    ++pop;
                }

            } else{
                if(!current_state && (neighbors_alive == 1 || neighbors_alive == 3)){
            //if it is dead and has three alive neighbors
                    updated.push_back({r, c});
                    current_state = true;
                } else if(current_state && (neighbors_alive == 0|| neighbors_alive == 2 || neighbors_alive == 4)){
                    updated.push_back({r, c});
                    current_state = false;
                }
                current_game[r][c].first = current_state;
                if(current_state){
                    //if it is now alive
                    if(starting_state){
                        //if it was alive
                        if(current_game[r][c].second == 1){
                            //if it has mutated
                            new_conway.push_back({r,c});
                            
                        }
                        ++current_game[r][c].second;
                    }
                    ++pop;
                }
            }
            
        }
    }

    for(int i = 0 ; i < new_conway.size() ; ++i){
        add_expanded_neighbors(new_conway[i].first, new_conway[i].second);
    }
    population = pop;
    return pop;
}


void update_neighbors(size_t r, size_t c, bool state) {
    //if the state is on, we want to increment, and decrement if off
    int difference = state? 1 : -1;

    if(current_game[r - 1][c - 1].second > 2)
        neighbor_num[r - 1][c - 1] += difference;
    if(current_game[r - 1][c + 1].second > 2)
        neighbor_num[r - 1][c + 1] += difference;
    if(current_game[r + 1][c - 1].second > 2)
        neighbor_num[r + 1][c - 1] += difference;
    if(current_game[r + 1][c + 1].second > 2)
        neighbor_num[r + 1][c + 1] += difference;



    neighbor_num[r - 1][c] += difference;
    neighbor_num[r][c - 1] += difference;
    neighbor_num[r][c + 1] += difference;
    neighbor_num[r + 1][c] += difference;
}

// ------------
// darwin_eval
// ------------

void life_eval_print(ostream& w, int iterations, int print, bool last) {

    for(size_t i = 0 ; i <= iterations ; ++i) {
        
        
        if(i % print == 0){
            w << "Generation = " << i <<", Population = "<< population <<"." << endl;
            print_grid(last && i == iterations);
            print_neighbors();
            if(!(last && i == iterations)) {
            w << endl;            
            }
        }
        evolve();
    }
    
}

// -------------
// darwin_solve
// -------------

void life_solve (istream& r, ostream& w) {

    //  read in num tests and get rid of empty line after it
    string dummy;
    getline(r, dummy);
    int num_tests = stoi(dummy);
    getline(r, dummy);


    for(size_t test = 0; test < num_tests; ++test) {
        pair<int, int> run_format = life_read(r, w);
        life_eval_print(w, run_format.first, run_format.second, test == num_tests - 1);

        w << flush;
        current_game = {};
        neighbor_num = {};
        updated = {};
        int population = 0;
    }
}
