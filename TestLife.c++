// ---------------
// TestLife.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Life.h"


using namespace std;

// -----------
// TestLife
// -----------

// ----
// read
// ----
TEST(LifeFixture, conwayCell_evolve0) {
    ConwayCell c(false);
    //it comes alive
    ASSERT_EQ(c.evolve(3),   true);
}

TEST(LifeFixture, conwayCell_evolve1) {
    ConwayCell c(true);
    //it dies
    ASSERT_EQ(c.evolve(1),   true);
}

TEST(LifeFixture, conwayCell_evolve_virt) {
    //Testing that the virtual function is working
    ConwayCell con = ConwayCell(false);
    AbstractCell* c = &con;
    //it comes alive
    ASSERT_EQ(c->evolve(3), true);
}


TEST(LifeFixture, fredkin_cell0) {
    FredkinCell c(false, 0);
    //it comes alive
    ASSERT_EQ(c.evolve(3),   true);
}

TEST(LifeFixture, fredkin_cell1) {
    FredkinCell c(true, 0);
    //it dies
    ASSERT_EQ(c.evolve(2),   true);
}

TEST(LifeFixture, fredkin_evolve_virt) {
    //Testing that the virtual function is working
    FredkinCell fred = FredkinCell(false, 0);
    AbstractCell* c = &fred;
    //it comes alive
    ASSERT_EQ(c->evolve(3),   true);
}

TEST(LifeFixture, cell0) {
    Cell c(new ConwayCell());
}

TEST(LifeFixture, cell1) {
    Cell c(new ConwayCell());
    Cell c2(c); //Cell(const Cell&)
    //checking that they each have their own.
    ASSERT_EQ(c.evolve(3), true);
    ASSERT_EQ(c2.evolve(2), false);

}

TEST(LifeFixture, cell2) {
    //Cell(Cell&&)
    Cell c(Cell(new ConwayCell(true)));
    ASSERT_EQ(c.evolve(2), false);

}

TEST(LifeFixture, diagonal0) {
    ConwayCell c = ConwayCell();
    ASSERT_EQ(c.diagonal_neighbors(), true);
}

TEST(LifeFixture, diagonal1) {
    FredkinCell c = FredkinCell();
    ASSERT_EQ(c.diagonal_neighbors(), false);
}

TEST(LifeFixture, diagonal2) {
    Cell c = Cell(new FredkinCell());
    ASSERT_EQ(c.diagonal_neighbors(), false);
}

TEST(LifeFixture, cell_mutate0) {
    Cell c(new FredkinCell(true, 0));
    ASSERT_EQ(c.diagonal_neighbors(), false);

    c.mutate();
    ASSERT_EQ(c.diagonal_neighbors(), true);
}

TEST(LifeFixture, cell_mutate1) {
    Cell c(new FredkinCell(true, 0));
    ASSERT_EQ(c.diagonal_neighbors(), false);

    c.mutate();
    ASSERT_EQ(c.show_state(), '*');
}




TEST(LifeFixture, show_state0) {
    FredkinCell c(true, 0);
    ASSERT_EQ(c.show_state(), '0');

    c = {false, 0};
    ASSERT_EQ(c.show_state(), '-');

    c = {true, 5};
    ASSERT_EQ(c.show_state(), '5');

    c = {true, 10};
    ASSERT_EQ(c.show_state(), '+');

    c = {true, 20};
    ASSERT_EQ(c.show_state(), '+');

}

TEST(LifeFixture, show_state1) {
    ConwayCell c(true);
    ASSERT_EQ(c.show_state(), '*');
    c = {false};
    ASSERT_EQ(c.show_state(), '.');
}

TEST(LifeFixture, show_state2) {
    Cell c(new ConwayCell(true));
    ASSERT_EQ(c.show_state(), '*');
}

TEST(LifeFixture, initialize_life0) {
    string str = "Generation = 0, Population = 0.\n----------\n----------\n----------\n----------\n----------\n----------\n----------\n----------\n----------\n----------\n";
    istringstream r("10 10\n0\n8 1\n");
    Life l(fredkin,r);
    ostringstream w;
    l.print_state(w, 0);
    ASSERT_EQ(str, w.str());
}

TEST(LifeFixture, initialize_life1) {
    string str = "Generation = 0, Population = 0.\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n";
    istringstream r("10 10\n0\n8 1\n");
    Life l(conway, r);
    ostringstream w;
    l.print_state(w, 0);
    ASSERT_EQ(str, w.str());
}

TEST(LifeFixture, initialize_life2) {
    string str = "Generation = 0, Population = 1.\n*..\n...\n...\n";
    istringstream r("3 3\n1\n0 0\n8 1\n");
    Life l(conway, r);
    ostringstream w;
    l.print_state(w, 0);
    ASSERT_EQ(str, w.str());
}


TEST(LifeFixture, life_init_alive0) {
    string str = "Generation = 0, Population = 1.\n0---------\n----------\n----------\n----------\n----------\n----------\n----------\n----------\n----------\n----------\n";
    istringstream r("10 10\n1\n0 0\n8 1\n");

    Life l(fredkin, r);
    ostringstream w;
    l.print_state(w, 0);
    //l.print_neighbors(cout);
    ASSERT_EQ(str, w.str());
}


TEST(LifeFixture, life_init_alive1) {
    string str = "Generation = 0, Population = 1.\n*.........\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n";
    istringstream r("10 10\n1\n0 0\n8 1\n");

    Life l(conway, r);
    ostringstream w;
    l.print_state(w, 0);
    ASSERT_EQ(str, w.str());
}

TEST(LifeFixture, solve) {
    istringstream r("1\n\n9 9\n3\n4 3\n4 4\n4 5\n3 1");
    ostringstream w;
    life_solve(cell, r, w);

    ASSERT_EQ("*** Life<Cell> 9x9 ***\n\nGeneration = 0, Population = 3.\n---------\n---------\n---------\n---------\n---000---\n---------\n---------\n---------\n---------\n\nGeneration = 1, Population = 10.\n---------\n---------\n---------\n---000---\n--01-10--\n---000---\n---------\n---------\n---------\n\nGeneration = 2, Population = 12.\n---------\n---------\n---000---\n---------\n-01*-*10-\n---------\n---000---\n---------\n---------\n\nGeneration = 3, Population = 28.\n---------\n---000---\n--01-10--\n-00-0-00-\n01-.-.-10\n-00-0-00-\n--01-10--\n---000---\n---------\n", w.str());
}
