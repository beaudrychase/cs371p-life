#include "FredkinCell.h"
#include <string>

using namespace std;

bool FredkinCell::evolve(int neighbor_num) {
    bool &current_state = AbstractCell::_state;
    bool resulting_state = current_state;
    //a dead cell becomes a live cell, if 1 or 3 neighbors are alive
    if(!resulting_state && (neighbor_num == 1 || neighbor_num == 3)) {
        resulting_state = true;
    }
    //a live cell becomes a dead cell, if 0, 2, or 4 neighbors are alive
    else if(resulting_state && (neighbor_num == 0 || neighbor_num == 2 || neighbor_num == 4)) {
        resulting_state = false;
    }

    if(current_state && resulting_state) {
        //this cell has stayed alive for a whole round.
        ++_age;
    }
    bool changed_state = current_state != resulting_state;
    current_state = resulting_state;
    return changed_state;
}

FredkinCell* FredkinCell::clone() const {
    return new FredkinCell(AbstractCell::_state, _age);
}

bool FredkinCell::diagonal_neighbors()const {
    return AbstractCell::diagonal_neighbors();
}

