#ifndef AbstractCell_h
#define AbstractCell_h

class AbstractCell {
protected:
    bool _state;
    bool _diagonal;
public:
    AbstractCell(bool s, bool dia):
        _state(s),
        _diagonal(dia)
    {}


    AbstractCell(const AbstractCell& a, bool diagonal):
        _state(a._state),
        _diagonal(diagonal)
    {}


    virtual ~AbstractCell() {}

    virtual AbstractCell* clone() const = 0;
    virtual bool evolve(int neighbor_num) = 0; // returns true if the state changes

    //if this cell is a type of cell that has diagonal neighbors this returns true
    virtual bool diagonal_neighbors() const {
        return _diagonal;
    }

    //creates a character representation of this cell.
    virtual inline char show_state() const {
        return 'x';
    };

    /*
    returns whether or not this cell is alive.
    */
    inline bool alive() const {
        return _state;
    }
};

#endif