#ifndef Cell_h
#define Cell_h
#include <string>
#include "AbstractCell.h"
#include "ConwayCell.h"
#include "LifeType.h"


class Cell {
private:
    AbstractCell* _p;
public:
    //since 2 of 3 cases it should be initialized as Fredkin, this default creates Fredkin
    Cell();

    //Based on the type of the game, it populates it with the right cell
    Cell(Life_type t);

    //if the AbstractCell was created outside of this
    Cell(AbstractCell* p);

    //creates a copy of the other cell
    Cell(const Cell& ref);

    //creates a copy of the other cell
    Cell(Cell&&);


    Cell& operator=(const Cell& rhs);
    Cell& operator=(Cell&& rhs);


    //destroys the cell that has been allocated to this
    ~Cell();

    inline bool evolve(int neighbor_num) {
        return _p->evolve(neighbor_num);
    }


    inline bool diagonal_neighbors() const {
        return _p->diagonal_neighbors();
    }
    inline void mutate() {
        ConwayCell* con = new ConwayCell(*_p);
        delete _p;
        _p = con;
    }
    inline char show_state() const {
        return _p->show_state();
    }

    inline bool alive() const {
        return _p->alive();
    }


};

#endif
